package classification;

import java.util.PriorityQueue;

/**
 *
 * @author Rafał
 */
public class kNNClassifier extends NNClassifier {
    int k;

    public kNNClassifier(int k) {
        super();
        this.k = k;
    }

    private class ClassifierElement implements Comparable<kNNClassifier.ClassifierElement> {

        final double distance;
        final int label;

        public ClassifierElement(double distance, int label) {
            this.distance = distance;
            this.label = label;
        }

        public int compareTo(kNNClassifier.ClassifierElement that) {
            return Double.valueOf(that.distance).compareTo(distance);
        }
    }

    @Override
    public ClassificationResults execute() {
        int[] ClassificationList = new int[TestSet[0].length];
        int[] results = {0, 0};
        for (int i = 0, l = TestSet[0].length; i < l; i++) {
            PriorityQueue<kNNClassifier.ClassifierElement> minDistance = new PriorityQueue<kNNClassifier.ClassifierElement>(this.k);
            for (int j = 0, lt = TrainingSet[0].length; j < lt; j++) {
                kNNClassifier.ClassifierElement tmp = new kNNClassifier.ClassifierElement(calculateDistanceBetweenSetsVectors(TestSet, i, TrainingSet, j), ClassLabels[j]);
                if (minDistance.size() < k) {
                    minDistance.offer(tmp);
                } else if (minDistance.peek().compareTo(tmp) < 0) {
                    minDistance.poll();
                    minDistance.offer(tmp);
                }
            }
            ClassificationList[i] = classifiy(minDistance);
            results[ClassificationList[i]]++;
        }
        return new ClassificationResults(ClassificationList, results);
    }

    private int classifiy(PriorityQueue<kNNClassifier.ClassifierElement> queue) {
        int[] classLabelProp = {0, 0};
        for (kNNClassifier.ClassifierElement elem : queue) {
            classLabelProp[elem.label]++;
        }
        return classLabelProp[0] > classLabelProp[1] ? 0 : 1;
    }
    
}
