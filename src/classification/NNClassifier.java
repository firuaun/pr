package classification;

/**
 *
 * @author Rafał
 */
public class NNClassifier extends Classifier {

    @Override
    public Classifier.ClassificationResults execute() {
        int[] ClassificationList = new int[TestSet[0].length];
        int[] results = {0,0};
        for(int i = 0, l = TestSet[0].length; i < l; i++) {
            double minDistance = Double.MAX_VALUE;
            for(int j = 0, lt = TrainingSet[0].length; j < lt; j++) {
                double tmpDistance = 0;
                if(minDistance > (tmpDistance = calculateDistanceBetweenSetsVectors(TestSet, i, TrainingSet, j))){
                    minDistance = tmpDistance;
                    ClassificationList[i] = ClassLabels[j]; 
                }
            }
            results[ClassificationList[i]]++;
        }
        return new Classifier.ClassificationResults(ClassificationList, results);
    }
    
    double calculateDistanceBetweenSetsVectors(double[][] a, int aColumnIndex, double[][] b, int bColumnIndex) {
        double distance = 0;
        for(int i = 0; i < a.length; i++){
            distance += Math.pow((b[i][bColumnIndex]-a[i][aColumnIndex]),2);
        }
        return Math.sqrt(distance);
    }
}