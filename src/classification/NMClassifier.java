package classification;

/**
 *
 * @author Rafał
 */
public class NMClassifier extends NNClassifier {

    @Override
    public ClassificationResults execute() {
        double[][] avgVector = new double[TrainingSet.length][2];
        int[] trainClassN = {0, 0};
        for (int i = 0, l = TrainingSet[0].length; i < l; i++) {
            for (int j = 0, m = TrainingSet.length; j < m; j++) {
                avgVector[j][ClassLabels[i]] += TrainingSet[j][i];
                trainClassN[ClassLabels[i]]++;
            }
        }
        for (int i = 0, l = TrainingSet.length; i < l; i++) {
            avgVector[i][0] = avgVector[i][0] / trainClassN[0];
            avgVector[i][1] = avgVector[i][1] / trainClassN[1];
        }
        int[] ClassificationList = new int[TestSet[0].length];
        int[] proportion = {0, 0};
        for (int i = 0, l = TestSet[0].length; i < l; i++) {
            double distanceA = calculateDistanceBetweenSetsVectors(TestSet, i, avgVector, 0);
            double distanceB = calculateDistanceBetweenSetsVectors(TestSet, i, avgVector, 1);
            if (distanceA < distanceB) {
                ClassificationList[i] = 0;
                proportion[0]++;
            } else if (distanceA > distanceB) {
                ClassificationList[i] = 1;
                proportion[1]++;
            }
        }
        return new ClassificationResults(ClassificationList, proportion);
    }
    
}
