package classification;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafał
 */
public class kNMClassifier extends kNNClassifier {
    final double MINIMAL_TRESHHOLD = 0.00001;
    final double POINT_DRAWING_CHANCE = 0.7;

    public kNMClassifier(int k) {
        super(k);
    }

    private void kMeansSetDetermination() {
        double[][][] means = new double[2][TrainingSet.length][this.k];
        double[][][] prevMeans = null;
        boolean cont = false;
        List<Integer>[][] dataPointClusters = (ArrayList<Integer>[][]) new ArrayList[2][this.k];
        for (int klasy = 0; klasy < 2; klasy++) {
            for (int klastry = 0; klastry < this.k; klastry++) {
                dataPointClusters[klasy][klastry] = new ArrayList<Integer>();
            }
        }
        for (int klasa = 0; klasa < 2; klasa++) {
            //clusters initialization - drawing k points and copy it to mean vectors
            int tmpMeansCount = 0;
            int i = 0;
            while (tmpMeansCount < this.k) {
                if (ClassLabels[i] == klasa && Math.random() > POINT_DRAWING_CHANCE) {
                    copyVector(TrainingSet, i, means[klasa], tmpMeansCount);
                    tmpMeansCount++;
                }
                i = (i + 1) % TrainingSet[0].length;
            }
        }
        do {
            if (cont) {
                prevMeans = copyMatrix(means);
            }
            //assaigning points to closer cluster
            for (int i = 0, l = TrainingSet[0].length; i < l; i++) {
                int klasa = ClassLabels[i];
                int closestMeanIndex = 0;
                double minDistance = Double.MAX_VALUE;
                double tmpDistance;
                for (int j = 0; j < means[klasa].length; j++) {
                    if (minDistance > (tmpDistance = calculateDistanceBetweenSetsVectors(TrainingSet, i, means[klasa], j))) {
                        minDistance = tmpDistance;
                        closestMeanIndex = j;
                    }
                }
                dataPointClusters[klasa][closestMeanIndex].add(i);
            }
            //calculating center of each cluster and class
            for (int klasa = 0; klasa < 2; klasa++) {
                for (int cluster = 0, l = dataPointClusters[klasa].length; cluster < l; cluster++) {
                    copyVector(calculateCenterForCluster(dataPointClusters[klasa][cluster]), means[klasa], cluster);
                }
            }
        } while (cont = !finishClusterization(means, prevMeans, MINIMAL_TRESHHOLD));
        int ind = 0;
        TrainingSet = new double[TrainingSet.length][means[0].length * 2];
        ClassLabels = new Integer[means[0].length * 2];
        for (int i = 0, l = means.length; i < l; i++) {
            for (int j = 0, n = means[i].length; j < n; j++) {
                ClassLabels[ind] = i;
                copyVector(means[i], j, TrainingSet, ind++);
            }
        }
    }

    @Override
    public ClassificationResults execute() {
        this.kMeansSetDetermination();
        return super.execute();
    }

    private static double[][] copyVector(double[] orig, double[][] dst, int j) {
        for (int a = 0, l = orig.length; a < l; a++) {
            dst[a][j] = orig[a];
        }
        return dst;
    }

    private static double[][][] copyMatrix(double[][][] matrix) {
        double[][][] copy = new double[matrix.length][matrix[0].length][matrix[0][0].length];
        for (int i = 0, l = matrix.length; i < l; i++) {
            for (int j = 0, q = matrix[i].length; j < q; j++) {
                for (int n = 0, m = matrix[i][j].length; n < m; n++) {
                    copy[i][j][n] = matrix[i][j][n];
                }
            }
        }
        return copy;
    }

    private static double[][] copyVector(double[][] orig, int i, double[][] dst, int j) {
        for (int a = 0, l = orig.length; a < l; a++) {
            dst[a][j] = orig[a][i];
        }
        return dst;
    }

    private double[] calculateCenterForCluster(List<Integer> indexes) {
        double[] result = new double[TrainingSet.length];
        for (int i = 0, l = result.length; i < l; i++) {
            double sum = 0;
            for (Integer index : indexes) {
                sum += TrainingSet[i][index];
            }
            result[i] = sum / indexes.size();
        }
        return result;
    }

    private boolean finishClusterization(double[][][] cur, double[][][] prev, double tresh) {
        if (prev == null) {
            return false;
        }
        double maxTresh = Double.NEGATIVE_INFINITY;
        double tmp;
        for (int i = 0, l = cur.length; i < l; i++) {
            for (int j = 0, q = cur[i].length; j < q; j++) {
                for (int n = 0, m = cur[i][j].length; n < m; n++) {
                    if (maxTresh < (tmp = Math.abs(cur[i][j][n] - prev[i][j][n]))) {
                        maxTresh = tmp;
                    }
                }
            }
        }
        return tresh >= maxTresh;
    }
    
}
