package classification;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafał
 */
public abstract class Classifier {
    
    public enum ClassifierType {NN,NM,kNN,kNM};
    
    double[][] TrainingSet, TestSet;
    Integer[] ClassLabels;
    Integer[] OrigTestClassLabels;
    public int[] RealClassLabelsTestSetProportions = {0,0};
    final int TRAIN_SET=0, TEST_SET=1;
    
    public class ClassificationResults{
        public final int[] ClassificationList;
        public final int[] ClassificationProportions;
        public ClassificationResults(int[] CL, int[] CP){
            ClassificationList = CL; ClassificationProportions = CP;
        }
    }
    
    public double efficency(ClassificationResults classificationResult) {
        int suma = 0;
        for(int i = 0, l = classificationResult.ClassificationList.length; i < l; i++) {
            if(classificationResult.ClassificationList[i] == this.OrigTestClassLabels[i]){
                suma++;
            }
        }
        return (double) suma / (double) classificationResult.ClassificationList.length;
    }
    
    public void generateTraining_and_Test_Sets(double[][] Dataset, String TrainSetSize, int[] OriginalClassLabels){
        int[] Index = new int[Dataset[0].length];
        double Th = Double.parseDouble(TrainSetSize)/100.0;
        int TrainCount=0, TestCount=0;
        List<Integer> classLabelsTmp = new ArrayList<Integer>();
        List<Integer> classLabelsTmpTest = new ArrayList<Integer>();
        for(int i=0; i<Dataset[0].length; i++) 
            if(Math.random()<=Th) {
                Index[i]=TRAIN_SET;
                TrainCount++;
                classLabelsTmp.add(OriginalClassLabels[i]);
            }
            else {
                Index[i]=TEST_SET;
                TestCount++;
                classLabelsTmpTest.add(OriginalClassLabels[i]);
                RealClassLabelsTestSetProportions[OriginalClassLabels[i]]++;
            }   
        TrainingSet = new double[Dataset.length][TrainCount];
        TestSet = new double[Dataset.length][TestCount];
        ClassLabels = classLabelsTmp.toArray(new Integer[TrainCount]);
        OrigTestClassLabels = classLabelsTmpTest.toArray(new Integer[TestCount]);
        TrainCount=0;
        TestCount=0;
        // label vectors for training/test sets
        for(int i=0; i<Dataset.length; i++){
            for(int j=0; j<Dataset[i].length; j++){
                if(Index[j]==TRAIN_SET){
                    TrainingSet[i][TrainCount++]=Dataset[i][j];
                }
                else
                    TestSet[i][TestCount++]=Dataset[i][j]; 
            }
            TrainCount=0;
            TestCount=0;
        }
    }
    
    abstract public ClassificationResults execute();
    
}
