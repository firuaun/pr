
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import Jama.*;
import classification.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/*
 * Some changes was made by firuaun.
 * Project is available on Beerware Licence.
 */

/*
 * PR_GUI.java
 *
 * Created on 2013-03-05, 19:40:56
 */

/**
 *
 * @author krzy
 */
public class PR_GUI extends javax.swing.JFrame {

    String InData; // dataset from a text file will be placed here
    int ClassCount=0, FeatureCount=0;
    double[][] F, FNew; // original feature matrix and transformed feature matrix
    int[] ClassLabels, SampleCount;
    String[] ClassNames;
    Classifier Cl;
    
    private class FLDValue {
        final private int[] indexes;
        final private double value;

        public FLDValue(int[] indexes, double value) {
            this.indexes = indexes;
            this.value = value;
        }
        public FLDValue(int index, double value) {
            this.indexes = new int[]{index};
            this.value = value;
        }
        
        public int[] getIndexes() {
            return indexes;
        }

        public double getValue() {
            return value;
        }
        
    }

    /** Creates new form PR_GUI */
    public PR_GUI() {
        initComponents();
        setSize(720,410);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbg_F = new javax.swing.ButtonGroup();
        b_read = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        l_dataset_name_l = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        l_dataset_name = new javax.swing.JLabel();
        l_nfeatures = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        selbox_nfeat = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        f_rb_extr = new javax.swing.JRadioButton();
        f_rb_sel = new javax.swing.JRadioButton();
        b_deriveFS = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        f_combo_criterion = new javax.swing.JComboBox();
        f_combo_PCA_LDA = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        tf_PCA_Energy = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        l_NewDim = new javax.swing.JLabel();
        sfsEnableCheckbox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        b_Train = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        tf_TrainSetSize = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        labelAName = new javax.swing.JLabel();
        labelBName = new javax.swing.JLabel();
        labelClassAValue = new javax.swing.JLabel();
        labelClassBValue = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        kValueBox = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        effLabel = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        l_FLD_winner = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        l_FLD_val = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        b_read.setText("Read dataset");
        b_read.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_readActionPerformed(evt);
            }
        });
        getContentPane().add(b_read);
        b_read.setBounds(20, 10, 130, 23);

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel1.setText("Dataset info");

        l_dataset_name_l.setText("Name:");

        jLabel3.setText("Classes:");

        jLabel4.setText("Features:");

        l_dataset_name.setText("...");

        l_nfeatures.setText("...");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(l_dataset_name_l)
                        .addGap(18, 18, 18)
                        .addComponent(l_dataset_name))
                    .addComponent(jLabel1))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(jLabel3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(l_nfeatures)))
                .addGap(100, 100, 100))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(l_dataset_name_l)
                    .addComponent(jLabel4)
                    .addComponent(l_dataset_name)
                    .addComponent(l_nfeatures))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(10, 50, 320, 80);

        jButton2.setText("Parse dataset");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(190, 10, 130, 23);

        jPanel3.setBackground(new java.awt.Color(255, 255, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel5.setText("Feature space");
        jPanel3.add(jLabel5);
        jLabel5.setBounds(14, 2, 118, 26);

        jLabel6.setText("FS Dimension");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(178, 9, 63, 14);

        selbox_nfeat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
        jPanel3.add(selbox_nfeat);
        selbox_nfeat.setBounds(268, 6, 40, 20);
        jPanel3.add(jSeparator1);
        jSeparator1.setBounds(14, 41, 290, 10);

        f_rb_extr.setBackground(new java.awt.Color(255, 255, 204));
        rbg_F.add(f_rb_extr);
        f_rb_extr.setText("Feature extraction");
        f_rb_extr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f_rb_extrActionPerformed(evt);
            }
        });
        jPanel3.add(f_rb_extr);
        f_rb_extr.setBounds(10, 110, 150, 23);

        f_rb_sel.setBackground(new java.awt.Color(255, 255, 204));
        rbg_F.add(f_rb_sel);
        f_rb_sel.setSelected(true);
        f_rb_sel.setText("Feature selection");
        f_rb_sel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                f_rb_selActionPerformed(evt);
            }
        });
        jPanel3.add(f_rb_sel);
        f_rb_sel.setBounds(10, 60, 140, 23);

        b_deriveFS.setText("Derive Feature Space");
        b_deriveFS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_deriveFSActionPerformed(evt);
            }
        });
        jPanel3.add(b_deriveFS);
        b_deriveFS.setBounds(10, 180, 292, 23);

        jLabel10.setText("Criterion");
        jPanel3.add(jLabel10);
        jLabel10.setBounds(200, 50, 41, 14);

        f_combo_criterion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Fisher discriminant", "Classification error" }));
        jPanel3.add(f_combo_criterion);
        f_combo_criterion.setBounds(160, 70, 140, 20);

        f_combo_PCA_LDA.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "PCA", "LDA" }));
        f_combo_PCA_LDA.setEnabled(false);
        jPanel3.add(f_combo_PCA_LDA);
        f_combo_PCA_LDA.setBounds(190, 110, 70, 20);

        jLabel12.setText("Energy");
        jPanel3.add(jLabel12);
        jLabel12.setBounds(20, 150, 34, 14);

        tf_PCA_Energy.setText("80");
        jPanel3.add(tf_PCA_Energy);
        tf_PCA_Energy.setBounds(70, 150, 30, 20);

        jLabel14.setText("%");
        jPanel3.add(jLabel14);
        jLabel14.setBounds(110, 150, 20, 14);

        jLabel15.setText("New dimension:");
        jPanel3.add(jLabel15);
        jLabel15.setBounds(160, 150, 75, 14);

        l_NewDim.setText("...");
        jPanel3.add(l_NewDim);
        l_NewDim.setBounds(270, 150, 30, 14);

        sfsEnableCheckbox.setBackground(new java.awt.Color(255, 255, 204));
        sfsEnableCheckbox.setText("SFS");
        jPanel3.add(sfsEnableCheckbox);
        sfsEnableCheckbox.setBounds(30, 80, 110, 23);

        getContentPane().add(jPanel3);
        jPanel3.setBounds(10, 140, 320, 220);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 156, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 126, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(530, 10, 160, 130);

        jPanel4.setBackground(new java.awt.Color(204, 255, 204));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(null);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel8.setText("Classifier");
        jPanel4.add(jLabel8);
        jLabel8.setBounds(10, 0, 79, 26);

        jLabel9.setText("Method");
        jPanel4.add(jLabel9);
        jLabel9.setBounds(14, 44, 36, 14);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nearest neighbor (NN)", "Nearest Mean (NM)", "k-Nearest Neighbor (k-NN)", "k-Nearest Mean (k-NM)" }));
        jPanel4.add(jComboBox2);
        jComboBox2.setBounds(74, 41, 152, 20);

        b_Train.setText("Train");
        b_Train.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_TrainActionPerformed(evt);
            }
        });
        jPanel4.add(b_Train);
        b_Train.setBounds(40, 130, 98, 23);

        jButton4.setText("Execute");
        jButton4.setEnabled(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ExecuteActionPerformed(evt);
            }
        });
        jPanel4.add(jButton4);
        jButton4.setBounds(210, 130, 96, 23);

        jLabel16.setText("Training part:");
        jPanel4.add(jLabel16);
        jLabel16.setBounds(20, 170, 80, 14);

        tf_TrainSetSize.setText("80");
        jPanel4.add(tf_TrainSetSize);
        tf_TrainSetSize.setBounds(110, 170, 20, 20);

        jLabel17.setText("%");
        jPanel4.add(jLabel17);
        jLabel17.setBounds(140, 170, 20, 14);

        labelAName.setText("A:");
        jPanel4.add(labelAName);
        labelAName.setBounds(40, 80, 60, 14);

        labelBName.setText("B:");
        jPanel4.add(labelBName);
        labelBName.setBounds(40, 100, 60, 14);

        labelClassAValue.setText("0");
        jPanel4.add(labelClassAValue);
        labelClassAValue.setBounds(120, 80, 100, 14);

        labelClassBValue.setText("0");
        jPanel4.add(labelClassBValue);
        labelClassBValue.setBounds(120, 100, 110, 14);

        jLabel7.setText("K");
        jLabel7.setMaximumSize(new java.awt.Dimension(42, 16));
        jLabel7.setMinimumSize(new java.awt.Dimension(42, 16));
        jPanel4.add(jLabel7);
        jLabel7.setBounds(260, 44, 20, 14);

        kValueBox.setText("5");
        jPanel4.add(kValueBox);
        kValueBox.setBounds(280, 41, 50, 20);

        jLabel11.setText("Eff:");
        jPanel4.add(jLabel11);
        jLabel11.setBounds(200, 90, 30, 14);

        effLabel.setText("...");
        jPanel4.add(effLabel);
        effLabel.setBounds(240, 90, 90, 14);

        getContentPane().add(jPanel4);
        jPanel4.setBounds(340, 150, 350, 210);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Results"));
        jPanel5.setLayout(null);

        jLabel2.setText("FS Winner:");
        jPanel5.add(jLabel2);
        jLabel2.setBounds(10, 30, 70, 14);

        l_FLD_winner.setText("xxx");
        jPanel5.add(l_FLD_winner);
        l_FLD_winner.setBounds(78, 30, 100, 14);

        jLabel13.setText("FLD value: ");
        jPanel5.add(jLabel13);
        jLabel13.setBounds(10, 60, 70, 14);

        l_FLD_val.setText("vvv");
        jPanel5.add(l_FLD_val);
        l_FLD_val.setBounds(78, 60, 100, 14);

        getContentPane().add(jPanel5);
        jPanel5.setBounds(340, 10, 190, 130);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void f_rb_selActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f_rb_selActionPerformed
        f_combo_criterion.setEnabled(true);
        sfsEnableCheckbox.setEnabled(true);
        f_combo_PCA_LDA.setEnabled(false);
    }//GEN-LAST:event_f_rb_selActionPerformed

    private void f_rb_extrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_f_rb_extrActionPerformed
        f_combo_criterion.setEnabled(false);
        sfsEnableCheckbox.setEnabled(false);
        f_combo_PCA_LDA.setEnabled(true);
    }//GEN-LAST:event_f_rb_extrActionPerformed

    private void b_readActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_readActionPerformed
        // reads in a text file; contents is placed into a variable of String type
        InData = readDataSet();
    }//GEN-LAST:event_b_readActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // Analyze text inputted from a file: determine class number and labels and number
        // of features; build feature matrix: columns - samples, rows - features
        try {
            if(InData!=null) {
                getDatasetParameters();
                l_nfeatures.setText(FeatureCount+"");
                fillFeatureMatrix();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this,ex.getMessage());
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void b_deriveFSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_deriveFSActionPerformed
        // derive optimal feature space
        if(F==null) return;
        if(f_rb_sel.isSelected()){
            // the chosen strategy is feature selection
            int[] flags = new int[FeatureCount];
            FNew = projectSampleFromFLDValue(selectFeatures(flags,Integer.parseInt((String)selbox_nfeat.getSelectedItem())));
        }
        else if(f_rb_extr.isSelected()){
            double TotEnergy=Double.parseDouble(tf_PCA_Energy.getText())/100.0;
            // Target dimension (if k>0) or flag for energy-based dimension (k=0)
            int k=0;
//            double[][] FF = { {1,1}, {1,2}};
//            double[][] FF = { {-2,0,2}, {-1,0,1}};
            // F is an array of initial features, FNew is the resulting array
            double[][] FFNorm = centerAroundMean(F); 
            Matrix Cov = computeCovarianceMatrix(FFNorm);
            Matrix TransformMat = extractFeatures(Cov,TotEnergy, k);     
            FNew = projectSamples(new Matrix(FFNorm),TransformMat);
            // FNew is a matrix with samples projected to a new feature space
            l_NewDim.setText(FNew.length+"");
        }
    }//GEN-LAST:event_b_deriveFSActionPerformed

    private void b_TrainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_TrainActionPerformed
        
        // first step: split dataset (in new feature space) into training / testing parts
        if(FNew==null) return; // no reduced feature space have been derived
        Classifier.ClassifierType which = Classifier.ClassifierType.values()[jComboBox2.getSelectedIndex()];
        switch(which){
            case NN: 
                Cl = new NNClassifier();
                break;
            case kNN:
                Cl = new kNNClassifier(Integer.parseInt(kValueBox.getText()));
                break;
            case NM: 
                Cl = new NMClassifier();
                break;
            case kNM:
                Cl = new kNMClassifier(Integer.parseInt(kValueBox.getText()));
                break;
        }
        Cl.generateTraining_and_Test_Sets(FNew, tf_TrainSetSize.getText(),ClassLabels);
        jButton4.setEnabled(true);
    }//GEN-LAST:event_b_TrainActionPerformed

    private void b_ExecuteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ExecuteActionPerformed
        if(Cl==null)
            return;
        Classifier.ClassificationResults results = Cl.execute();
        labelClassAValue.setText(String.format("%d (%d)", results.ClassificationProportions[0], Cl.RealClassLabelsTestSetProportions[0]));
        labelClassBValue.setText(String.format("%d (%d)", results.ClassificationProportions[1], Cl.RealClassLabelsTestSetProportions[1]));
        effLabel.setText(String.format("%.0f%%", Cl.efficency(results)*100));
    }//GEN-LAST:event_b_ExecuteActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PR_GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_Train;
    private javax.swing.JButton b_deriveFS;
    private javax.swing.JButton b_read;
    private javax.swing.JLabel effLabel;
    private javax.swing.JComboBox f_combo_PCA_LDA;
    private javax.swing.JComboBox f_combo_criterion;
    private javax.swing.JRadioButton f_rb_extr;
    private javax.swing.JRadioButton f_rb_sel;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField kValueBox;
    private javax.swing.JLabel l_FLD_val;
    private javax.swing.JLabel l_FLD_winner;
    private javax.swing.JLabel l_NewDim;
    private javax.swing.JLabel l_dataset_name;
    private javax.swing.JLabel l_dataset_name_l;
    private javax.swing.JLabel l_nfeatures;
    private javax.swing.JLabel labelAName;
    private javax.swing.JLabel labelBName;
    private javax.swing.JLabel labelClassAValue;
    private javax.swing.JLabel labelClassBValue;
    private javax.swing.ButtonGroup rbg_F;
    private javax.swing.JComboBox selbox_nfeat;
    private javax.swing.JCheckBox sfsEnableCheckbox;
    private javax.swing.JTextField tf_PCA_Energy;
    private javax.swing.JTextField tf_TrainSetSize;
    // End of variables declaration//GEN-END:variables

    private String readDataSet() {

        String s_tmp, s_out="";
        JFileChooser jfc = new JFileChooser();
        jfc.setCurrentDirectory(new File("."));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                                            "Datasets - plain text files", "txt");
        jfc.setFileFilter(filter);
        if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(jfc.getSelectedFile()));
                while((s_tmp=br.readLine())!=null) s_out += s_tmp + '$';
                br.close();
                l_dataset_name.setText(jfc.getSelectedFile().getName());
            } catch (Exception e) {        }
        }
        return s_out;
    }

    private void getDatasetParameters() throws Exception{
        // based on data stored in InData determine: class count and names, number of samples 
        // and number of features; set the corresponding variables
        String stmp=InData, saux;
        // analyze the first line and get feature count: assume that number of features
        // equals number of commas
        saux = InData.substring(InData.indexOf(',')+1, InData.indexOf('$'));
        if(saux.length()==0) throw new Exception("The first line is empty");
        // saux stores the first line beginning from the first comma
        int count=0;
        while(saux.indexOf(',') >0){
            saux = saux.substring(saux.indexOf(',')+1);            
            count++;
        }
        FeatureCount = count+1; // the first parameter
        // Determine number of classes, class names and number of samples per class
        boolean New;
        int index=-1;
        List<String> NameList = new ArrayList<String>();
        List<Integer> CountList = new ArrayList<Integer>();
        List<Integer> LabelList = new ArrayList<Integer>();
        while(stmp.length()>1){
            saux = stmp.substring(0,stmp.indexOf(' '));
            New = true; 
            index++; // new class index
            for(int i=0; i<NameList.size();i++) 
                if(saux.equals(NameList.get(i))) {
                    New=false;
                    index = i; // class index
                }
            if(New) {
                NameList.add(saux);
                CountList.add(0);
            }
            else{
                CountList.set(index, CountList.get(index).intValue()+1);
            }           
            LabelList.add(index); // class index for current row
            stmp = stmp.substring(stmp.indexOf('$')+1);
        }
        // based on results of the above analysis, create variables
        ClassNames = new String[NameList.size()];
        for(int i=0; i<ClassNames.length; i++)
            ClassNames[i]=NameList.get(i);
        labelAName.setText(ClassNames[0]);
        labelBName.setText(ClassNames[1]);
        SampleCount = new int[CountList.size()];
        for(int i=0; i<SampleCount.length; i++)
            SampleCount[i] = CountList.get(i).intValue()+1;
        ClassLabels = new int[LabelList.size()];
        for(int i=0; i<ClassLabels.length; i++)
            ClassLabels[i] = LabelList.get(i).intValue();
    }

    private void fillFeatureMatrix() throws Exception {
        // having determined array size and class labels, fills in the feature matrix
        int n = 0;
        String saux, stmp = InData;
        for(int i=0; i<SampleCount.length; i++)
            n += SampleCount[i];
        if(n<=0) throw new Exception("no samples found");
        F = new double[FeatureCount][n]; // samples are placed column-wise
        for(int j=0; j<n; j++){
            saux = stmp.substring(0,stmp.indexOf('$'));
            saux = saux.substring(stmp.indexOf(',')+1);
            for(int i=0; i<FeatureCount-1; i++) {
                F[i][j] = Double.parseDouble(saux.substring(0,saux.indexOf(',')));
                saux = saux.substring(saux.indexOf(',')+1);
            }
            F[FeatureCount-1][j] = Double.parseDouble(saux);
            stmp = stmp.substring(stmp.indexOf('$')+1);
        }
        int cc = 1;
    }
    
    private int[] range(int start, int end) {
        int rangeLength = end-start;
        int[] result = new int[rangeLength];
        for(int i = 0; i < rangeLength; i++){
            result[i] = start + i;
        }
        return result;
    }
    
    private int[] nextCombination(int[] combination, int k, int n){
        int currentIndex = k - 1;
        int maxValue = n;
        int[] nextCombination = Arrays.copyOf(combination, combination.length);
        while(currentIndex >= 0){
            if(nextCombination[currentIndex] < maxValue){
                nextCombination[currentIndex]++;
                currentIndex = -1;
            }
            else {
                --currentIndex;
                --maxValue;
                if(currentIndex >= 0 && nextCombination[currentIndex+1]-nextCombination[currentIndex] > 1){
                    int incrementValue = nextCombination[currentIndex] + 2;
                    for(int i = currentIndex+1; i < k; i++){
                        nextCombination[i] = incrementValue++;
                    }
                }
            }
        }
        if(maxValue == n - k){
            return new int[0];
        }
        return nextCombination;
    }

    private FLDValue selectFeature1D() {
        double FLD=0, tmp;
        int max_ind=-1;        
        for(int i=0; i<FeatureCount; i++){
            if((tmp=computeFisherLD(F[i]))>FLD){
                FLD=tmp;
                max_ind = i+1;
            }
        }
        return new FLDValue(max_ind,FLD);
    }
    private FLDValue selectFeatureND(int d) {
        double FLD=0, tmp;
        int tries = 0;
        int[] max_ind = new int[d];
        Arrays.fill(max_ind, -1);
        int[] combination = range(0,d); // prepare to make combination without repetitions

        do {
            ++tries;
            double[][] matrix = new double[d][F[0].length];
            for(int i = 0; i < d; i++) {
                matrix[i] = Arrays.copyOf(F[combination[i]], F[combination[i]].length);
            }
            if((tmp = computeFisherLD(matrix, d))>FLD) {
                FLD = tmp;
                max_ind = combination;
            }
        }while((combination = nextCombination(combination, d, FeatureCount-1)).length >= d);
        return new FLDValue(max_ind, FLD);
    }
    
    private FLDValue selectFeatureSFS(int d){
        int tmpD = 1;
        double FLD=0,tmp;
        Integer max_ind = -1;
        FLDValue result = selectFeature1D();
        int[] combination = new int[d];
        combination[0] = result.getIndexes()[0];
        Set<Integer> leftIndexes = new HashSet<Integer>(FeatureCount-1);
        for(int e: range(0,FeatureCount)){
            if(combination[0]!=e){
                leftIndexes.add(e);
            }
        }
        while(++tmpD <= d){
            for(Integer e: leftIndexes){
                combination[tmpD-1]= e;
                //
                double[][] matrix = new double[d][F[0].length];
                for(int i = 0; i < tmpD; i++) {
                    matrix[i] = Arrays.copyOf(F[combination[i]], F[combination[i]].length);
                }
                if((tmp = computeFisherLD(matrix, tmpD))>FLD) {
                    FLD = tmp;
                    max_ind = e;
                    result = new FLDValue(combination.clone(), FLD);
                }
                //
            }
            combination = result.getIndexes().clone();
            leftIndexes.remove(max_ind);
        }
        return result;
    }
    
    private FLDValue selectFeatures(int[] flags, int d) {
        FLDValue winner;
            long startTime = System.nanoTime();
        winner = d == 1 ? 
                selectFeature1D() : sfsEnableCheckbox.isSelected() ?
                    selectFeatureSFS(d) : selectFeatureND(d);
        JOptionPane.showMessageDialog(rootPane, "Time elapsed: "+TimeUnit.MILLISECONDS.convert(System.nanoTime()-startTime,TimeUnit.NANOSECONDS)+"ms","Results", JOptionPane.INFORMATION_MESSAGE);
        Arrays.sort(winner.getIndexes());
        l_FLD_winner.setText(Arrays.toString(winner.getIndexes())+"");
        l_FLD_val.setText(Math.round(winner.getValue()*100d)/100d+"");
        return winner;
    }
    
    /**
     * Compute Fisher for d > 1
     * @param vec matrix with samples
     * @return computed fisher value
     */
    private double computeFisherLD(double[][] vec, int d) {
        double[] mA = new double[d], mB = new double[d], mDif = new double[d];
        double[][] mAHelper = new double[d][SampleCount[0]], mBHelper = new double[d][SampleCount[1]];
        double[][] sA = new double[d][SampleCount[0]], sB =new double[d][SampleCount[1]];
        int k,m;
        for(int f = 0; f < d; f++) {
            k=0;m=0;
            for(int i =0, l = vec[f].length; i < l; i++){
                if(ClassLabels[i]==0){
                    sA[f][k] = vec[f][i];
                    mA[f] += sA[f][k++];
                }
                else {
                    sB[f][m] = vec[f][i];
                    mB[f] += sB[f][m++];
                }
            }
            mA[f] /= SampleCount[0];
            Arrays.fill(mAHelper[f], mA[f]);
            mB[f] /= SampleCount[1];
            Arrays.fill(mBHelper[f], mB[f]);
            mDif[f] = mA[f] - mB[f];
        }
        Matrix sAMatrix = (new Matrix(sA)).minus(new Matrix(mAHelper));
        sAMatrix = sAMatrix.times(sAMatrix.transpose());
        Matrix sBMatrix = (new Matrix(sB)).minus(new Matrix(mBHelper));
        sBMatrix = sBMatrix.times(sBMatrix.transpose());
        double mSumModule = 0;
        for(double tmp : mDif) {
            mSumModule += tmp*tmp;
        }
        return Math.sqrt(mSumModule)/(sAMatrix.det()+sBMatrix.det());
    }
    
    
    private double computeFisherLD(double[] vec) {
        // 1D, 2-classes
        double mA=0, mB=0, sA=0, sB=0;
        for(int i=0; i<vec.length; i++){
            if(ClassLabels[i]==0) {
                mA += vec[i];
                sA += vec[i]*vec[i];
            }
            else {
                mB += vec[i];
                sB += vec[i]*vec[i];
            }
        }
        mA /= SampleCount[0];
        mB /= SampleCount[1];
        sA = sA/SampleCount[0] - mA*mA;
        sB = sB/SampleCount[1] - mB*mB;
        return Math.abs(mA-mB)/(Math.sqrt(sA)+Math.sqrt(sB));
    }

    private Matrix extractFeatures(Matrix C, double Ek, int k) {               
        
        Matrix evecs, evals;
        // compute eigenvalues and eigenvectors
        evecs = C.eig().getV();
        evals = C.eig().getD();
        
        // PM: projection matrix that will hold a set dominant eigenvectors
        Matrix PM;
        if(k>0) {
            // preset dimension of new feature space
//            PM = new double[evecs.getRowDimension()][k];
            PM = evecs.getMatrix(0, evecs.getRowDimension()-1, 
                    evecs.getColumnDimension()-k, evecs.getColumnDimension()-1);
        }
        else {
            // dimension will be determined based on scatter energy
            double TotEVal = evals.trace(); // total energy
            double EAccum=0;
            int m=evals.getColumnDimension()-1;
            while(EAccum<Ek*TotEVal){
                EAccum += evals.get(m, m);
                m--;
            }
            PM = evecs.getMatrix(0, evecs.getRowDimension()-1,m+1,evecs.getColumnDimension()-1);
        }

/*            System.out.println("Eigenvectors");                
            for(int i=0; i<r; i++){
                for(int j=0; j<c; j++){
                    System.out.print(evecs[i][j]+" ");
                }
                System.out.println();                
            }
            System.out.println("Eigenvalues");                
            for(int i=0; i<r; i++){
                for(int j=0; j<c; j++){
                    System.out.print(evals[i][j]+" ");
                }
                System.out.println();                
            }
*/
        
        return PM;
    }

    private Matrix computeCovarianceMatrix(double[][] m) {
//        double[][] C = new double[M.length][M.length];
        
        Matrix M = new Matrix(m);
        Matrix MT = M.transpose();       
        Matrix C = M.times(MT);
        return C;
    }

    private double[][] centerAroundMean(double[][] M) {
        
        double[] mean = new double[M.length];
        for(int i=0; i<M.length; i++)
            for(int j=0; j<M[0].length; j++)
                mean[i]+=M[i][j];
        for(int  i=0; i<M.length; i++) mean[i]/=M[0].length;
        for(int i=0; i<M.length; i++)
            for(int j=0; j<M[0].length; j++)
                M[i][j]-=mean[i];
        return M;
    }

    private double[][] projectSamples(Matrix FOld, Matrix TransformMat) {
        
        return (FOld.transpose().times(TransformMat)).transpose().getArrayCopy();
    }
    
    private double[][] projectSampleFromFLDValue(FLDValue winner){
        double[][] result = new double[winner.indexes.length][F[0].length];
        for(int i= 0, l = winner.indexes.length; i < l; i++){
            result[i] = Arrays.copyOf(F[winner.indexes[i]], F[winner.indexes[i]].length);
        }
        return result;
    }
}
