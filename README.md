# Aplikacja na statystyczne przetwarzanie danych

* enabled FS Dimension > 1 (max. 5)
* added SFS
* classifying (NN, kNN, NM) for selection and extraction

## Licence

Project is available under "THE BEER-WARE LICENSE" (Revision 42).